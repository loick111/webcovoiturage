<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace system;

/**
 * Description of Session
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Session {
    /**
     *
     * @var \system\Database
     */
    private $database;
    
    private $config;
    
    /**
     *
     * @var \system\input\Input
     */
    private $input;
    
    private $account;
    
    private $SESSID = null;
    
    public function __construct(\system\Database $database, \system\Config $config, \system\input\Input $input) {
        $this->database = $database;
        $this->config = $config;
        $this->input = $input;
        $this->_loadData();
    }
    
    private function _loadData(){
        $account = $this->database->selectFirst('SELECT A.* FROM SESSION_STORAGE S JOIN ACCOUNT A ON A.USER_ID = S.USER_ID WHERE SESS_ID = ?', $this->getSESSID());
        
        if($account){
            $this->account = $account;
        }
    }
    
    public function getSESSID(){
        if($this->SESSID === null){
            if($this->input->cookie->get($this->config->cookie_name) === null){
                $this->SESSID = $this->_generateSESSID();
                $this->input->cookie->setCookie($this->config->cookie_name, $this->SESSID, time() + 30 * 3600 * 24);
            }else
                $this->SESSID = $this->input->cookie->get($this->config->cookie_name);
        }
        
        return $this->SESSID;
    }
    
    private function _generateSESSID(){
        return md5(uniqid());
    }
    
    public function clear(){
        $this->database->executeUpdate('DELETE FROM SESSION_STORAGE WHERE SESS_ID = ?', $this->getSESSID());
        $this->userId = null;
    }
    
    public function isLogged(){
        return !empty($this->account);
    }
    
    public function getUserId(){
        return $this->getAccountVar('USER_ID');
    }
    
    public function getAccountVar($name){
        return isset($this->account[strtoupper($name)]) ? $this->account[strtoupper($name)] : null;
    }
    
    public function getUserMail(){
        return $this->getAccountVar('MAIL');
    }
    
    public function getUserFirstName(){
        return $this->getAccountVar('USER_FIRSTNAME');
    }
    
    public function getUserLastName(){
        return $this->getAccountVar('USER_LASTNAME');
    }
    
    public function login($user_id){
        $exists = $this->database->selectFirst('SELECT COUNT(*) FROM SESSION_STORAGE WHERE SESS_ID = ?', $this->getSESSID())['COUNT(*)'] > 0;
        
        if(!$exists){
            $this->database->executeUpdate('INSERT INTO SESSION_STORAGE(SESS_ID, USER_ID) VALUES(?,?)', $this->getSESSID(), $user_id);
        }else{
            $this->database->executeUpdate('UPDATE SESSION_STORAGE SET USER_ID = ? WHERE SESS_ID = ?', $user_id, $this->getSESSID());
        }
    }
}
