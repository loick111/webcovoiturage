<section id="cgu">
    <h1>Conditions Generales d'Utilisation</h1>
    <article>
        <h2>Copyright</h2>
        Tous les éléments de ce site, y compris les documents téléchargeables, sont libres de droit. A l’exception de l’iconographie, la reproduction des pages de ce site est autorisée à la condition d’y mentionner la source. Elles ne peuvent être utilisées à des fins commerciales et publicitaires.
    </article>
    <article>
        <h2>Fiabilité</h2>
        Les informations et/ou documents figurant sur ce site et/ou accessibles par ce site proviennent de sources considérées comme étant fiables.
        Toutefois, ces informations et/ou documents sont susceptibles de contenir des inexactitudes techniques et des erreurs typographiques.
        Les admininstrateurs se réserve le droit de les corriger, dès que ces erreurs sont portées à sa connaissance.
        Il est fortement recommandé de vérifier l’exactitude et la pertinence des informations et/ou documents mis à disposition sur ce site.
        Les informations et/ou documents disponibles sur ce site sont susceptibles d’être modifiés à tout moment, et peuvent avoir fait l’objet de mises à jour. En particulier, ils peuvent avoir fait l’objet d’une mise à jour entre le moment de leur téléchargement et celui où l’utilisateur en prend connaissance.

        L’utilisation des informations et/ou documents disponibles sur ce site se fait sous l’entière et seule responsabilité de l’utilisateur, qui assume la totalité des conséquences pouvant en découler, sans que Webvoiturage puisse être recherché à ce titre, et sans recours contre ce dernier.
    </article>
    <article>
        <h2>Disponibilité</h2>
        Les développeurs s’efforcent de permettre l’accès au site 24 heures sur 24, 7 jours sur 7, sauf en cas de force majeure ou d’un événement hors du contrôle des développeurs, et sous réserve des éventuelles pannes et interventions de maintenance nécessaires au bon fonctionnement du site et des services.
        Par conséquent, nous ne pouvopns garantir une disponibilité du site et/ou des services, une fiabilité des transmissions et des performances en terme de temps de réponse ou de qualité. Il n’est prévu aucune assistance technique vis à vis de l’utilisateur que ce soit par des moyens électronique ou téléphonique.
        La responsabilité de Webvoiturage ne saurait être engagée en cas d’impossibilité d’accès à ce site et/ou d’utilisation des services.
        Par ailleurs, les développeurs peuvent être amenés à interrompre le site ou une partie des services, à tout moment sans préavis, le tout sans droit à indemnités.
    </article>

    <article>
        <h2>Données utilisateur</h2>
        L’utilisateur reconnaît et accepte que Webvoiturage ne soit pas responsable des interruptions, et des conséquences qui peuvent en découler pour l’utilisateur ou tout tiers.
        L’utilisateur déclare accepter les caractéristiques et les limites d’Internet, et notamment reconnaît que :
        Webvoiturage n’assume aucune responsabilité sur les services accessibles par Internet et n’exerce aucun contrôle de quelque forme que ce soit sur la nature et les caractéristiques des données qui pourraient transiter par l’intermédiaire de son centre serveur.
        L’utilisateur reconnaît que les données circulant sur Internet ne sont pas protégées notamment contre les détournements éventuels. La présence du logo institue une présomption simple de validité. La communication de toute information jugée par l’utilisateur de nature sensible ou confidentielle se fait à ses risques et périls.
        L’utilisateur reconnaît que les données circulant sur Internet peuvent être réglementées en termes d’usage ou être protégées par un droit de propriété.
        L’utilisateur est seul responsable de l’usage des données qu’il consulte, interroge et transfère sur Internet.
        L’utilisateur reconnaît que Webvoiturage ne dispose d’aucun moyen de contrôle sur le contenu des services accessibles sur Internet.
    </article>
</section>