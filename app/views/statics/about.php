<section id="about">
    <h1>A Propos...</h1>
    <article>
        <strong>Webvoiturage</strong> est un site qui permet de participer à des covoiturages entre des membres du personnel de l'université d'Aix-Marseille.
        Il a été réalisé au cours d'un projet à L'I.U.T. informatique d'Aix-en-Provence.
    </article>
    <article>
        Ce site a été développé par<strong> Loïck Mahieux</strong>, <strong>Vincent Quatrevieux</strong>, <strong>Maxime Mille</strong>, <strong>Olivier Richit</strong>, <strong>Raphaël Kumar</strong>, <strong>Benjamin Piat</strong> et <strong>Rémy Kaloustian</strong>.
    </article>
</section>