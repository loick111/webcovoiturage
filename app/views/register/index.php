<h1>Inscription</h1>

<section id = "register">
    <form method="post" action="<?php echo $this->url('register/perform.php')?>" id="reg_form">
        <table>
            <tr>
                <td><label for= "first_name" class="required">Prénom</label></td>
                <td><?php echo $form->getFirstName()->getHTML()?></td>
            </tr>
            <tr>
                <td><label for="last_name" class="required">Nom</label></td>
                <td><?php echo $form->getLastName()->getHTML()?></td>
            </tr>
            <tr>
                <td><label for="job" class="required">Statut</label></td>
                <td><?php echo $form->getStatus()->getHTML()?></td>
            </tr>
            <tr>
                <td><label for="mail" class="required">Adresse e-mail</label></td>
                <td><?php echo $form->getMail()->getHTML()?></td>
            </tr>
            <tr>
                <td><label for="pass" class="required">Mot de passe</label></td>
                <td><?php echo $form->getPass()->getHTML()?></td>
            </tr>
            <tr>
                <td><label for="pass2" class="required">Confirmation</label></td>
                <td><?php echo $form->getPass2()->getHTML()?></td>
            </tr>
        </table>
        <p id="reg_info">Les champs précédés de * sont obligatoire</p>
        <input type="submit" value="S'inscrire" />
    </form>
    <?php echo $this->js('form')?>
    <?php echo $this->js('register')?>
    <script src="<?php echo $this->url('register/script.js')?>" type="text/javascript"></script>
</section>
