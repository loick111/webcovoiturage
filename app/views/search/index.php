<section id="title">
    <h1>Recherche de covoiturage</h1>
</section>

<section id="search">
    <form method="get" action="<?php echo $this->helpers->url('search/redirect.php')?>">
        <div class="row">
            <label for="search_from">De :</label>
            <input type="text" name="from" id="search_from" required/>
        </div>
        <div class="row">
            <label for="search_to">Vers :</label>
            <input type="text" name="to" id="search_to" required/>
        </div>
        <input type="submit" value="Rechercher"/>
    </form>
</section>

<?php echo $this->helpers->js('search')?>