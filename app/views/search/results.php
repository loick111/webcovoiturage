<h1>Resultats de la recherche</h1>

<section id="search">
    <form method="get" action="<?php echo $this->helpers->url('search/redirect.php')?>">
        <div class="row">
            <div class="table">
                <label for="search_from">De :</label>
                <div><input type="text" name="from" id="search_from" value="<?php echo $from?>" required/></div>
            </div>
        </div>
        <div class="row">
            <div class="table">
                <label for="search_to">Vers :</label>
                <div><input type="text" name="to" id="search_to" value="<?php echo $to?>" required/></div>
                <div style="width: 26px;"><input type="submit" value=""/></div>
            </div>
        </div>
    </form>
</section>

<section id="results">
    <?php if(empty($results)):?>
    <p class="error">Pas de résultats</p>
    <?php else:?>
    <?php foreach($results as $trajet):?>
    <article class="result">
        <h2><a href="<?php echo $this->secureUrl('trajets', 'show', $trajet['TRAJET_ID'])?>"><?php echo $trajet['DEP_NAME'], ' &gt; ', $trajet['ARR_NAME']?></a></h2>
        <div class="infos">
            <div class="info">
                <span class="what">Nombre de places maximum</span>
                <span class="value"><?php echo $trajet['MAXIMUM'] ?></span>
            </div>
            <div class="info">
                <span class="what">Nombre de places libre</span>
                <span class="value"><?php echo $trajet['FREE_PLACES']?></span>
            </div>
            <div class="info">
                <span class="what">Utilisateur</span>
                <span class="value"><?php echo ucfirst($trajet['user']['USER_FIRSTNAME']), ' ', strtoupper($trajet['user']['USER_LASTNAME']) ?></span>
            </div>
            <div class="info">
                <span class="what">Date</span>
                <span class="value">le <?php echo date('d / m / Y à G \h i', $trajet['DATE_TRAJET']) ?></span>
            </div>
        </div>
        <aside>
            <img src="https://maps.googleapis.com/maps/api/staticmap?size=128x128&amp;markers=color:blue|label:D|<?php echo $trajet['LAT_DEP'], ',', $trajet['LONG_DEP']?>&amp;markers=color:red|label:A|<?php echo $trajet['LAT_ARR'], ',', $trajet['LONG_ARR']?>"/>
        </aside>
    </article>
    <?php endforeach?>
    <?php endif?>
</section>
<?php echo $this->js('results')?>