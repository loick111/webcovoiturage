<section id = "contact">
    <p id = "intro">Vous pouvez nous contacter par mail </p>
    <p id = "mail">webco@gmail.com</p>
    <p id = "social"> ...Ou à travers les réseaux sociaux<p>
    <?php echo $this->helpers->img('facebook.png', 'Facebook'); ?>
    <?php echo $this->helpers->img('twitter.png', 'Twitter'); ?>
    <?php echo $this->helpers->img('google_plus.png', 'Google+'); ?>
</section>

<div id = "object">
    <p id = "pobject">  - Pensez à bien préciser le domaine concerné par votre message (proposition de covoiturage, recherche de covoiturage, règlement, litige, disposition du site, 
                    problème d'accès, autre).<br/>
             - Veuillez rester poli dans votre message, il est inutile d'espérer qu'on vous réponde si vous vous montrez haineux ou faites absence de tout sens logique.
    </p>
</div>