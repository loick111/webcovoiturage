<section id="contact">
    <h1>Nous contacter</h1>
    <form method="post" action="<?php echo $this->helpers->url('contact/perform') ?>">
        <table>
            <tr>
                <td><label for="firstname">Prénom</label></td>
                <td><?php echo $message->getFirstName()->getHTML() ?></td>
            </tr>
            <tr>
                <td><label for="lastname">Nom</label></td>
                <td><?php echo $message->getLastName()->getHTML() ?></td>
            </tr>
            <tr>
                <td><label for="mail">Mail</label></td>
                <td><?php echo $message->getMail()->getHTML() ?></td>
            </tr>
            <tr>
                <td><label for="message">Message</label></td>
                <td><textarea name="message" class="required"></textarea></td>
            </tr>
        </table>
        <input type="submit" value="Envoyer"/>
    </form>
</section>