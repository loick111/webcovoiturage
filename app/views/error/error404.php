<!DOCTYPE html>

<!-- Page d'erreur 404 - TERMINEE
    -affiche une page d'erreur
    -ne dis pas à l'utilisateur d'aller se faire mettre (on avait dit du tact monsieur Quatrevieux)
    -affiche un petit bouton swaggy de retour à l'accueil.


-->
<html>
    <head>
        <meta charset="utf-8" />
			<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
		<link rel="stylesheet" href="error404.css"/>
        <title>Page introuvable</title>
    </head>
	
	<body>
		
		<div id="error">
			<h1> Nous sommes désolés, cette page est introuvable...</h1>
			
			<ul> ---> Ce que vous devriez faire : <br/>
			
				<li>Essayer de rafraîchir la page (actualiser ou f5); </li>
				<li>Vérifier que la syntaxe de l'adresse est la bonne (auriez-vous oublié un 'w', un '/'); </li>
				<li> Vérifier la connexion au réseau de votre ordinateur; </li>
				<li>Assurez vous que votre navigateur autorise l'accès à cette page; </li>
			</ul>
		</div>
		
		<div >
			<a href="index.html" id= "retour">accueil </a>
		<div/>
	</body>