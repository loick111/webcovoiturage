<div id="container">
    <header>
        <h1>Erreur 404 : Page introuvable</h1>
    </header>
    <section id="debug_info">
        La page que vous avez demandé n'existe pas.
        <h2>Message :</h2>
        <article id="message">
            <?php echo $message?>
        </article>
    </section>
</div>