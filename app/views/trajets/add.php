<section id="trajets">
    <h1>Ajouter un trajet</h1>
    <form method="post" action="<?php echo $this->helpers->url('trajets/add') ?>" id="traj_form">
        <table>
            <tr>
                <td><label for="date">Date</label></td>
                <td><?php echo $form->getDate()->getHTML() ?></td>
            </tr>
            <tr>
                <td><label for="depart">Départ</label></td>
                <td><?php echo $form->getDepart()->getHTML() ?></td>
            </tr>
            <tr>
                <td><label for="arrivee">Arrivée</label></td>
                <td><?php echo $form->getArrivee()->getHTML() ?></td>
            </tr>
            <tr>
                <td><label for="places">Nombre de places</label></td>
                <td><?php echo $form->getPlaces()->getHTML() ?></td>
            </tr>
        </table>
        <input type="submit" name="submit" value="Ajouter" />
    </form>
</section>
<?php echo $this->js('form')?>
<script src="<?php echo $this->url('trajets/scriptadd.js')?>"></script>
<?php echo $this->helpers->js('add_trajet')?>