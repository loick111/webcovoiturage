<h1>Mes trajets</h1>

<section id="results">
    <h2 style="color: darkgreen;margin-left: 25px;">Organisés</h2>
    <?php if(empty($organized)):?>
    <p class="error">Pas de trajets</p>
    <?php else:?>
    <?php foreach($organized as $trajet):?>
    <article class="result">
        <h2><a href="<?php echo $this->secureUrl('trajets', 'show', $trajet['TRAJET_ID'])?>"><?php echo $trajet['DEP_NAME'], ' &gt; ', $trajet['ARR_NAME']?></a></h2>
        <div class="infos">
            <div class="info">
                <span class="what">Nombre de places maximum</span>
                <span class="value"><?php echo $trajet['MAXIMUM'] ?></span>
            </div>
            <div class="info">
                <span class="what">Nombre de places libre</span>
                <span class="value"><?php echo $trajet['FREE_PLACES']?></span>
            </div>
            <div class="info">
                <span class="what">Utilisateur</span>
                <span class="value"><?php echo ucfirst($trajet['USER_FIRSTNAME']), ' ', strtoupper($trajet['USER_LASTNAME']) ?></span>
            </div>
            <div class="info">
                <span class="what">Date</span>
                <span class="value">le <?php echo date('d / m / Y à G \h i', $trajet['DATE_TRAJET']) ?></span>
            </div>
        </div>
        <aside>
            <img src="https://maps.googleapis.com/maps/api/staticmap?size=128x128&amp;markers=color:blue|label:D|<?php echo $trajet['LAT_DEP'], ',', $trajet['LONG_DEP']?>&amp;markers=color:red|label:A|<?php echo $trajet['LAT_ARR'], ',', $trajet['LONG_ARR']?>"/>
        </aside>
    </article>
    
    <?php endforeach?>
    <?php endif?>
    <h2 style="color: darkgreen;margin-left: 25px;">Participés</h2>
    <?php if(empty($part)):?>
    <p class="error">Pas de trajets</p>
    <?php else:?>
    <?php foreach($part as $trajet):?>
    <article class="result">
        <h2><a href="<?php echo $this->secureUrl('trajets', 'show', $trajet['TRAJET_ID'])?>"><?php echo $trajet['DEP_NAME'], ' &gt; ', $trajet['ARR_NAME']?></a></h2>
        <div class="infos">
            <div class="info">
                <span class="what">Nombre de places maximum</span>
                <span class="value"><?php echo $trajet['MAXIMUM'] ?></span>
            </div>
            <div class="info">
                <span class="what">Nombre de places libre</span>
                <span class="value"><?php echo $trajet['FREE_PLACES']?></span>
            </div>
            <div class="info">
                <span class="what">Utilisateur</span>
                <span class="value"><?php echo ucfirst($trajet['USER_FIRSTNAME']), ' ', strtoupper($trajet['USER_LASTNAME']) ?></span>
            </div>
            <div class="info">
                <span class="what">Date</span>
                <span class="value">le <?php echo date('d / m / Y à G \h i', $trajet['DATE_TRAJET']) ?></span>
            </div>
        </div>
        <aside>
            <img src="https://maps.googleapis.com/maps/api/staticmap?size=128x128&amp;markers=color:blue|label:D|<?php echo $trajet['LAT_DEP'], ',', $trajet['LONG_DEP']?>&amp;markers=color:red|label:A|<?php echo $trajet['LAT_ARR'], ',', $trajet['LONG_ARR']?>"/>
        </aside>
    </article>
    <?php endforeach?>
    <?php endif?>
</section>