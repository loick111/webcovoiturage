<section id="show">
    <h1><?php echo $trajet['DEP_NAME'] ?> &gt; <?php echo $trajet['ARR_NAME'] ?></h1>
    <img src="https://maps.googleapis.com/maps/api/staticmap?size=500x500&amp;markers=color:blue|label:D|<?php echo $trajet['LAT_DEP'], ',', $trajet['LONG_DEP']?>&amp;markers=color:red|label:A|<?php echo $trajet['LAT_ARR'], ',', $trajet['LONG_ARR']?>"/>
    <div id="infos">
        <div class="info">
            <p>Nombre de places maximum</p>
            <p><?php echo $trajet['MAXIMUM'] ?></p>
        </div>
        <div class="info">
            <p>Nombre de places libre</p>
            <p><?php echo $trajet['FREE_PLACES']?></p>
        </div>
        <div class="info">
            <p>Utilisateur</p>
            <p><a href="mailto:<?php echo $trajet['MAIL'] ?>"><?php echo $trajet['USER_FIRSTNAME'] ?> <?php echo $trajet['USER_LASTNAME'] ?></a></p>
        </div>
        <div class="info">
            <p>Date</p>
            <p><?php echo date('d/m/Y à G\hi', $trajet['DATE_TRAJET']) ?></p>
        </div>
        <div class="info">
            <p>Participants</p>
            <?php if(empty($trajet['reserves'])): ?>
            <p>Il n'y a pas de participants pour l'instant</p>
            <?php else: ?>
            <?php foreach($trajet['reserves'] as $reserve): ?>
                <p><a href="mailto:<?php echo $reserve['user']['MAIL'] ?>"><?php echo $reserve['user']['USER_FIRSTNAME'] ?> <?php echo $reserve['user']['USER_LASTNAME'] ?></a></p>
            <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
    <ul class="nav_buttons">
        <?php if($canReserve):?>
            <li><a class="button" href="<?php echo $this->helpers->secureUrl('trajets', 'reserve', $trajet['TRAJET_ID']) ?>">Réserver</a></li>
        <?php endif?>
        <?php if($b):?>
            <li><a class="button" href="<?php echo $this->helpers->secureUrl('trajets', 'unreserve', $trajet['TRAJET_ID']) ?>">Annuler res.</a></li>
        <?php endif?>
        <li><a href="<?php echo $this->helpers->secureUrl('search' , 'results', $trajet['DEP_NAME'], $trajet['ARR_NAME'])?>" class="button">Autres trajets</a></li>
    </ul>
</section>