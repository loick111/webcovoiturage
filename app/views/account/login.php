<h1>Connexion</h1>
<section id="login">
    <form method="post" action="<?php echo $this->url('account/performlogin.php')?>" id="login_form">
        <span id="login_error"><?php echo $error == 'error' ? 'Échec d\'authentification' : ''?></span>
        <input placeholder="mail AMU" type="text" name= "mail" value=""/>
        <input placeholder="mot de passe" type="password" name= "password" value=""/>
        <input type="submit" value="Connexion"/>
    </form>
</section>