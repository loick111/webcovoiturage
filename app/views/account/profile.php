<h1>Mon Compte</h1>

<section id="profile">
	<form method="post" action="" >
		<table>
			<tr>
				<td><label for="firstname" class="required">Prénom</label></td>
				<td><?php echo $form->getFirstName()->getHTML() ?></td>
			</tr>
			<tr>
				<td><label for="lastname" class="required">Nom</label></td>
				<td><?php echo $form->getLastName()->getHTML() ?></td>
			</tr>
			<tr>
				<td><label for="mail" class="required">Mail</label></td>
				<td><?php echo $form->getMail()->getHTML() ?></td>
			</tr>
			<tr>
				<td><label for="address" class="required">Adresse</label></td>
				<td><?php echo $form->getAddress()->getHTML() ?></td>
			</tr>
			<tr>
				<td><label for="city" class="required">Ville</label></td>
				<td><?php echo $form->getCity()->getHTML() ?></td>
			</tr>
			<tr>
				<td><label for="postal_code" class="required">Code Postal</label></td>
				<td><?php echo $form->getPostalCode()->getHTML() ?></td>
			</tr>
			<tr>
				<td><label for="status" class="required">Status</label></td>
				<td><?php echo $form->getStatus()->getHTML() ?></td>
			</tr>
			<tr>
				<td><label for="pass" class="required">Mot de passe</label></td>
				<td><?php echo $form->getPass()->getHTML() ?></td>
			</tr>
			<tr>
				<td><label for="pass2" class="required">Confirmer le mot de passe</label></td>
				<td><?php echo $form->getPass2()->getHTML() ?></td>
			</tr>
		</table>
		<p id="reg_info">Les champs précédés de * sont obligatoire</p>
		<input type="submit" value="Modifier" />
	</form>
</section>
<?php echo $this->js('form')?>
<script src="<?php echo $this->url('account/profilescript.js')?>"></script>

