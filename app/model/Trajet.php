<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace app\model;

/**
 * Description of Trajet
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Trajet extends \system\mvc\Model {
    /**
     *
     * @var \app\model\GoogleMapAPI
     */
    private $api;

    private $account;
    
    public function __construct(\system\Database $db, \app\model\GoogleMapAPI $api, \app\model\Account $account) {
        parent::__construct($db);
        $this->api = $api;
        $this->account = $account;
    }

    private function _distance($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return ($miles * 1.609344);
    }

    public function addTrajet ($user_id, $depart, $arrive, $datetrajet, $maximum) {
        $this->db->executeUpdate('INSERT INTO TRAJET (DEPART, ARRIVE, DATE_TRAJET, USER_ID, MAXIMUM) VALUE (?,?,?,?,?)',$depart, $arrive, $datetrajet, $user_id, $maximum);
        return $this->db->lastInsertId();
    }
    
    public function getTrajet($id){
        return $this->db->selectFirst(
            'SELECT T.*, (MAXIMUM - COUNT(R.id)) AS FREE_PLACES, L1.LATITUDE AS LAT_DEP, L1.LONGITUDE AS LONG_DEP, L1.NAME AS DEP_NAME, L2.LATITUDE AS LAT_ARR, L2.LONGITUDE AS LONG_ARR, L2.NAME AS ARR_NAME, USER_FIRSTNAME, USER_LASTNAME, MAIL FROM TRAJET T '
                . 'JOIN LIEU L1 ON DEPART = L1.LIEU_ID '
                . 'JOIN LIEU L2 ON ARRIVE = L2.LIEU_ID '
                . 'JOIN ACCOUNT A ON A.USER_ID = T.USER_ID '
                . 'LEFT JOIN RESERVER R ON trajet = TRAJET_ID '
                . 'WHERE TRAJET_ID = ?', $id);
    }
    
    public function getOrganizedTrajects($user_id){
        $data = $this->db->selectAll('SELECT T.*, (MAXIMUM - COUNT(R.id)) AS FREE_PLACES, L1.LATITUDE AS LAT_DEP, L1.LONGITUDE AS LONG_DEP, L1.NAME AS DEP_NAME, L2.LATITUDE AS LAT_ARR, L2.LONGITUDE AS LONG_ARR, L2.NAME AS ARR_NAME, USER_FIRSTNAME, USER_LASTNAME, MAIL FROM TRAJET T '
                . 'JOIN LIEU L1 ON DEPART = L1.LIEU_ID '
                . 'JOIN LIEU L2 ON ARRIVE = L2.LIEU_ID '
                . 'JOIN ACCOUNT A ON A.USER_ID = T.USER_ID '
                . 'LEFT JOIN RESERVER R ON trajet = TRAJET_ID '
                . 'WHERE T.USER_ID = ?', $user_id);
        
        $ret = array();
        
        foreach ($data as $t){
            if(!empty($t['TRAJET_ID']))
                $ret[] = $t;
        }
        
        return $ret;
    }
    
    public function unreserve($trajet, $user){
        $this->db->executeUpdate('DELETE FROM RESERVER WHERE trajet = ? AND user = ?', $trajet, $user);
    }
    
    public function getParticipatedTrajects($user_id){
        $data = $this->db->selectAll('SELECT T.*, (MAXIMUM - COUNT(R.id)) AS FREE_PLACES, L1.LATITUDE AS LAT_DEP, L1.LONGITUDE AS LONG_DEP, L1.NAME AS DEP_NAME, L2.LATITUDE AS LAT_ARR, L2.LONGITUDE AS LONG_ARR, L2.NAME AS ARR_NAME, USER_FIRSTNAME, USER_LASTNAME, MAIL FROM TRAJET T '
                . 'JOIN LIEU L1 ON DEPART = L1.LIEU_ID '
                . 'JOIN LIEU L2 ON ARRIVE = L2.LIEU_ID '
                . 'JOIN ACCOUNT A ON A.USER_ID = T.USER_ID '
                . 'LEFT JOIN RESERVER R ON trajet = TRAJET_ID '
                . 'WHERE TRAJET_ID IN ( SELECT trajet FROM RESERVER WHERE user = ?)', $user_id);
        
        $ret = array();
        
        foreach ($data as $t){
            if(!empty($t['TRAJET_ID']))
                $ret[] = $t;
        }
        
        return $ret;
    }

    public function trouverTrajet($depart, $arrive){
        $geoDepart = $this->api->getLongLat($depart);
        $geoArrive = $this->api->getLongLat($arrive);

        if(!$geoArrive || !$geoDepart)
            return array();

        $stmt = $this->db->query(
            'SELECT T.*, (MAXIMUM - COUNT(R.id)) AS FREE_PLACES, L1.LATITUDE AS LAT_DEP, L1.LONGITUDE AS LONG_DEP, L1.NAME AS DEP_NAME, L2.LATITUDE AS LAT_ARR, L2.LONGITUDE AS LONG_ARR, L2.NAME AS ARR_NAME FROM TRAJET T '
                . 'JOIN LIEU L1 ON DEPART = L1.LIEU_ID '
                . 'JOIN LIEU L2 ON ARRIVE = L2.LIEU_ID '
                . 'LEFT JOIN RESERVER R ON trajet = TRAJET_ID '
                . 'GROUP BY TRAJET_ID');

        $results = [];

        while($trajet = $stmt->fetch()){
            $dist1 = $this->_distance($geoDepart['lat'], $geoDepart['lng'], $trajet['LAT_DEP'], $trajet['LONG_DEP']);
            $dist2 = $this->_distance($geoArrive['lat'], $geoArrive['lng'], $trajet['LAT_ARR'], $trajet['LONG_ARR']);

            if($dist1 < 2 && $dist2 < 2){
                $results[] = $trajet;
            }
        }

        return $results;
    }
    
    public function delTrajet($id) {
        return $this->db->executeUpdate('DELETE FROM TRAJET WHERE TRAJET_ID = ?', $id);
    }

    public function getReserves($id) {
        $sql = 'SELECT * FROM RESERVER '
            . 'WHERE trajet = ?';

        $res = $this->db->selectAll($sql, $id);

        foreach($res as &$reserve)
            $reserve['user'] = $this->account->findUserById($reserve['user']);

        return $res;
    }

    public function getReserve($user, $trajet) {
        $sql = 'SELECT * FROM RESERVER '
            . 'WHERE trajet = ? '
            . 'AND user = ?';
        $res = $this->db->selectFirst($sql, $trajet, $user);

        if(!empty($res))
            return true;
        return false;
    }

    public function addReserve($user, $trajet) {
        $sql = 'INSERT INTO RESERVER (user, trajet) VALUES (?,?)';
        return $this->db->executeUpdate($sql, $user, $trajet);
    }
    
    public function getFreePlaces($trajet){
        $data = $this->db->selectFirst('SELECT MAXIMUM - COUNT(R.id) AS FREE_PLACES FROM TRAJET JOIN REVERVER R ON trajet = TRAJET_ID WHERE TRAJET_ID = ?', $trajet);
        
        if($data === null)
            return null;
        
        return $data['FREE_PLACES'];
    }
}
