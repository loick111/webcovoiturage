<?php

/*
 * The MIT License
 *
 * Copyright 2014 aze.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace app\model;

/**
 * Description of Account
 *
 * @author aze
 */
class Account extends \system\mvc\Model {
    public function findUserByMail($mail) {
        return $this->db->selectFirst('SELECT * FROM ACCOUNT WHERE MAIL = ?', $mail);
    }
      public function findUserById ($user_id) {
        return $this->db->selectFirst('SELECT * FROM ACCOUNT WHERE USER_ID = ?', $user_id);
    }
    
    public function findUserByMailAndPassword ($mail, $password) {
        return $this->db->selectFirst('SELECT * FROM ACCOUNT WHERE MAIL = ? AND PASSWORD = ?', $mail, $password);
    }
    
    public function addUser ($mail, $password, $firstname, $lastname, $status, $sexe, $age) {
        return $this->db->executeUpdate('INSERT INTO ACCOUNT (MAIL, PASSWORD, USER_FIRSTNAME, USER_LASTNAME, STATUS, SEXE, AGE) VALUE (?,?,?,?,?,?,?)', $mail, $password, $firstname, $lastname, $status, $sexe, $age);
    }
    
    public function delUser ($user_id) {
        return $this->db->selectFirst('DELETE FROM TRAJET WHERE USER_ID = ?; DELETE FROM VOITURE WHERE USER_ID = ?; DELETE FROM ACCOUNT WHERE USER_ID = ?', $user_id ,$user_id, $user_id);
    }
    public function updateUser ($user_id, $mail, $permis, $firstname, $lastname, $password, $status, $sexe, $age) {
        return $this->db->executeUpdate('UPDATE ACCOUNT SET PERMIS = ?, MAIL = ?, FIRST_NAME = ?, LAST_NAME = ?, PASSWORD = ?, SATUS = ?, SEXE = ?, AGE = ? WHERE USER_ID = ?', $permis, $mail, $firstname, $lastname, $password, $status, $sexe, $age, $user_id);
    }
    
    public function getVoiture ($user_id) {
        return $this->db->selectFirst('SELECT * FROM VOITURE WHERE USER_ID = ?', $user_id);
    }
    
    public function updateVoiture ($user_id, $model, $id_immat, $capacite) {
        return $this->db->executeUpdate('UPDATE VOITURE SET ID_IMMAT = ?, MODELE = ?, CAPACITE = ? WHERE USER_ID = ?',$id_immat, $model, $capacite, $user_id);
    }
    public function addVoiture ($user_id,$model, $id_immat, $capacite) {
        return $this->db->executeUpdate('INSERT INTO VOITURE (ID_IMMAT,MODEL, USER_ID, CAPACITE) VALUE (?,?,?,?)', $id_immat, $model, $user_id, $capacite);
    }
    public function delVoiture ($id_immat) {
        return $this->db->executeUpdate('DELETE FROM VOITURE WHERE ID_IMMAT = ?', $id_immat);
    }
    
    
    public function modifierComplet($complet, $user_id, $depart, $arrive, $datetrajet){
        return $this->db->executeUpdate('UPDATE TRAJET SET COMPLET = ? WHERE DEPART = ? AND ARRIVE = ? AND DATE_TRAJET = ? AND USER_ID = ?', $complet, $depart, $arrive, $datetrajet, $user_id);
    }
    
    public function addEtape($id_lieu, $user_id, $depart, $arrive, $datetrajet){
        return $this->db->executeUpdate('INSERT INTO ARRET (LIEU_ID, TRAJET_DEP, TRAJET_ARR, USER_ID, DATE_TRAJET) VALUE (?,?,?,?,?)' ,$id_lieu, $depart, $arrive, $user_id, $datetrajet);
    }
    
    public function delEtape($id_lieu, $user_id, $depart, $arrive, $datetrajet){
        return $this->db->executeUpdate('DELETE FROM ARRET WHERE LIEU_ID = ? AND TRAJET_DEP = ? AND TRAJET_ARR = ? AND DATE_TRAJET = ? AND USER_ID = ?', $id_lieu,$depart, $arrive, $datetrajet, $user_id);
    }
    
    public function foundEtape($user_id, $depart, $arrive, $datetrajet){
        return $this->db->selectAll('SELECT LIEU_ID FROM ARRET WHERE TRAJET_DEP = ? AND TRAJET_ARR = ? AND DATE_TRAJET = ? AND USER_ID = ?', $depart, $arrive, $datetrajet, $user_id);    
    }
    
    public function mailExists($mail){
        return $this->db->selectFirst('SELECT COUNT(*) FROM ACCOUNT WHERE MAIL = ?', $mail)['COUNT(*)'] > 0;
    }
}
