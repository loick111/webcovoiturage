<?php

/*
 * The MIT License
 *
 * Copyright 2015 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace app\model;

/**
 * Description of Lieu
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Lieu extends \system\mvc\Model {
    /**
     *
     * @var \app\model\GoogleMapAPI
     */
    private $api;
    
    public function __construct(\system\Database $db, \app\model\GoogleMapAPI $api) {
        parent::__construct($db);
        $this->api = $api;
    }
    
    public function createLieu($name, $lat, $long){
        $this->db->executeUpdate('INSERT INTO LIEU (NAME, LATITUDE, LONGITUDE) VALUES(?,?,?)', $name, $lat, $long);
        return $this->db->lastInsertId();
    }
    
    public function getLieuById($id){
        return $this->db->selectFirst('SELECT * FROM LIEU WHERE LIEU_ID = ?', $id);
    }
    
    public function getLieuIdByName($name){
        $lieu = $this->db->selectFirst('SELECT LIEU_ID FROM LIEU WHERE NAME = ?', $name);
        
        if($lieu)
            return $lieu['LIEU_ID'];
        
        $geo = $this->api->getLongLat($name);
        
        if(!$geo)
            return null;
        
        return $this->createLieu($name, $geo['lat'], $geo['lng']);
    }
}
