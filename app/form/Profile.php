<?php

/*
 * The MIT License
 *
 * Copyright 2014 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace app\form;

/**
 * Description of Register
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Profile extends \system\form\Form {
    private $firstName;
    private $lastName;
    private $mail;
    private $address;
    private $city;
    private $postal_code;
    private $status;
    private $pass;
    private $pass2;
    
    public function __construct(\system\input\Input $input, \system\helper\Url $url) {
        parent::__construct($input, $url);
        $this->firstName = new \system\form\field\Input($this, 'first_name');
        $this->lastName = new \system\form\field\Input($this, 'last_name');
        $this->mail = new \system\form\field\Input($this, 'mail');
        $this->address= new \system\form\field\Input($this, 'address');
        $this->city = new \system\form\field\Input($this, 'city');
        $this->postal_code= new \system\form\field\Input($this, 'postal_code');

        $this->status = new \system\form\field\Select($this, 'job', array(
            '--- Choisir ---' => '',
            'Professeur' => 'prof',
            'Étudiant' => 'etu',
            'Autre' => 'other'
        ));
        $this->pass = new \system\form\field\Input($this, 'pass');
        $this->pass2 = new \system\form\field\Input($this, 'pass2');
        $this->makeFields();
    }
    
    private function makeFields(){
        //add attributes
        $this->mail->setAttribute('type', 'email');
        $this->pass->setAttribute('type', 'password');
        $this->pass2->setAttribute('type', 'password');
        
        //required rule
        $required = new \system\form\rule\Required();
        $this->firstName->addRule($required);
        $this->lastName->addRule($required);
        $this->mail->addRule($required);
        $this->address->addRule($required);
        $this->city->addRule($required);
        $this->postal_code->addRule($required);
        $this->status->addRule($required);
        $this->pass->addRule($required);
        $this->pass2->addRule($required);

        //name regex rule
        $nameRule = new \system\form\rule\Regex('/^[a-zéèàïë -]{3,52}$/i');
        $this->firstName->addRule($nameRule);
        $this->lastName->addRule($nameRule);
        
        //pass rules
        $length = new \system\form\rule\Length(6, 32);
        $this->pass->addRule($length);
        $this->pass2->addRule($length);
        $this->pass2->addRule(new \system\form\rule\EqualOther($this->pass, 'La confirmation du mot de passe ne correspond pas'));
        
        //test mail
        $this->mail->addRule(new \system\form\rule\Callback(function($value, &$error) /*use($model)*/{
            $a = array('test.test@univ-amu.fr');
            
            // $model->mailExists($value);
            
            if(in_array($value, $a)){
                $error = 'Mail déjà enregistré !';
                return false;
            }
            
            return true;
        }));
        $this->mail->addRule(new \system\form\rule\Regex('/[a-z0-9_\.-]+@(etu\.)?univ-amu\.fr/i', 'Le mail doit être un mail de l\'amu (@univ-amu.fr ou @etu.univ-amu.fr).'));
        
        //handle fields
        $this->addField($this->firstName);
        $this->addField($this->lastName);
        $this->addField($this->mail);
        $this->addField($this->address);
        $this->addField($this->city);
        $this->addField($this->postal_code);
        $this->addField($this->status);
        $this->addField($this->pass);
        $this->addField($this->pass2);
    }
    
    public function getID() {
        return 'profile';
    }

    public function getSubmitURL() {
        return 'profile/perform/ajax';
    }

    public function getValidationURL() {
        return 'profile/validate/';
    }

    //getters
    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getMail() {
        return $this->mail;
    }

    public function getPass() {
        return $this->pass;
    }

    public function getPass2() {
        return $this->pass2;
    }

    /**
     * @return \system\form\field\Input
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return \system\form\field\Input
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return \system\form\field\Input
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

}
