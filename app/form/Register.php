<?php

/*
 * The MIT License
 *
 * Copyright 2014 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace app\form;

/**
 * Description of Register
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Register extends \system\form\Form {
    private $firstName;
    private $lastName;
    private $status;
    private $mail;
    private $pass;
    private $pass2;
    
    /**
     *
     * @var \app\model\Account
     */
    private $model;
    
    public function __construct(\system\input\Input $input, \system\helper\Url $url, \app\model\Account $model) {
        parent::__construct($input, $url);
        $this->model = $model;
        $this->firstName = new \system\form\field\Input($this, 'first_name');
        $this->lastName = new \system\form\field\Input($this, 'last_name');
        $this->status = new \system\form\field\Select($this, 'job', array(
            '--- Choisir ---' => '',
            'Professeur' => 'prof',
            'Étudiant' => 'etu',
            'Autre' => 'other'
        ));
        $this->mail = new \system\form\field\Input($this, 'mail');
        $this->pass = new \system\form\field\Input($this, 'pass');
        $this->pass2 = new \system\form\field\Input($this, 'pass2');
        $this->makeFields();
    }
    
    private function makeFields(){
        //add attributes
        $this->mail->setAttribute('type', 'email');
        $this->pass->setAttribute('type', 'password');
        $this->pass2->setAttribute('type', 'password');
        
        //required rule
        $required = new \system\form\rule\Required();
        $this->firstName->addRule($required);
        $this->lastName->addRule($required);
        $this->mail->addRule($required);
        $this->pass->addRule($required);
        $this->pass2->addRule($required);
        $this->status->addRule($required);
        
        //name regex rule
        $nameRule = new \system\form\rule\Regex('/^[a-zéèàïë -]{3,52}$/i');
        $this->firstName->addRule($nameRule);
        $this->lastName->addRule($nameRule);
        
        //pass rules
        $length = new \system\form\rule\Length(6, 32);
        $this->pass->addRule($length);
        $this->pass2->addRule($length);
        $this->pass2->addRule(new \system\form\rule\EqualOther($this->pass, 'La confirmation du mot de passe ne correspond pas'));
        
        //test mail
        $this->mail->addRule(new \system\form\rule\Callback(function($value){
            return !$this->model->mailExists($value);
        }, 'Le mail est déjà existant.'));
        $this->mail->addRule(new \system\form\rule\Regex('/[a-z0-9_\.-]+@(etu\.)?univ-amu\.fr/i', 'Le mail doit être un mail de l\'amu (@univ-amu.fr ou @etu.univ-amu.fr).'));
        
        //handle fields
        $this->addField($this->firstName);
        $this->addField($this->lastName);
        $this->addField($this->mail);
        $this->addField($this->pass);
        $this->addField($this->pass2);
        $this->addField($this->status);
    }
    
    public function getID() {
        return 'reg_form';
    }

    public function getSubmitURL() {
        return 'register/perform/ajax';
    }

    public function getValidationURL() {
        return 'register/validate/';
    }

    //getters
    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getMail() {
        return $this->mail;
    }

    public function getPass() {
        return $this->pass;
    }

    public function getPass2() {
        return $this->pass2;
    }

}
