<?php

/*
 * The MIT License
 *
 * Copyright 2014 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace app\form;

/**
 * Description of Register
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Contact extends \system\form\Form {
    private $firstName;
    private $lastName;
    private $mail;
    private $message;
    
    public function __construct(\system\input\Input $input, \system\helper\Url $url) {
        parent::__construct($input, $url);
        $this->firstName = new \system\form\field\Input($this, 'firstname');
        $this->lastName = new \system\form\field\Input($this, 'lastname');
        $this->mail = new \system\form\field\Input($this, 'mail');
        $this->message= new \system\form\field\Input($this, 'message');
        $this->makeFields();
    }
    
    private function makeFields(){
        //add attributes
        $this->firstName->setAttribute('type', 'text');
        $this->lastName->setAttribute('type', 'text');
        $this->mail->setAttribute('type', 'email');
        $this->message->setAttribute('type', 'text');

        //required rule
        $required = new \system\form\rule\Required();
        $this->firstName->addRule($required);
        $this->lastName->addRule($required);
        $this->mail->addRule($required);
        $this->message->addRule($required);

        //name regex rule
        $nameRule = new \system\form\rule\Regex('/^[a-zéèàïë -]{3,52}$/i');
        $this->firstName->addRule($nameRule);
        $this->lastName->addRule($nameRule);

        //mail regex rule
        $this->mail->addRule(new \system\form\rule\Regex('/[a-z0-9_\.-]+@(etu\.)?univ-amu\.fr/i', 'Le mail doit être un mail de l\'amu (@univ-amu.fr ou @etu.univ-amu.fr).'));
        
        //handle fields
        $this->addField($this->firstName);
        $this->addField($this->lastName);
        $this->addField($this->mail);
        $this->addField($this->message);
    }
    
    public function getID() {
        return 'contact';
    }

    public function getSubmitURL() {
        return 'contact/perform';
    }

    public function getValidationURL() {
        return 'contact/validate/';
    }

    //getters
    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getMail() {
        return $this->mail;
    }

    public function getMessage() {
        return $this->message;
    }

}
