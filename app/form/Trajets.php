<?php

/*
 * The MIT License
 *
 * Copyright 2014 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace app\form;

use system\form\rule\Callback;

/**
 * Class Profile
 * @package app\form
 */
class Trajets extends \system\form\Form {

    private $date;
    private $depart;
    private $arrivee;
    private $places;

    /**
     * @var \app\model\Lieu
     */
    private $lieu;

    public function __construct(\system\input\Input $input, \system\helper\Url $url, \app\model\Lieu $lieu) {
        parent::__construct($input, $url);
        $this->lieu = $lieu;

        $this->date = new \system\form\field\Input($this, 'date');
        $this->depart = new \system\form\field\Input($this, 'depart');
        $this->arrivee = new \system\form\field\Input($this, 'arrivee');
        $this->places = new \system\form\field\Input($this, 'places');
        $this->makeFields();
    }

    private function makeFields() {
        //add attributes
        $this->date->setAttribute('type', 'text');
        $this->date->setAttribute('placeholder', 'jj/mm/aaaa hh:mm');
        $this->depart->setAttribute('type', 'text');
        $this->arrivee->setAttribute('type', 'text');
        $this->places->setAttribute('type', 'number');

        //required rule
        $required = new \system\form\rule\Required();
        $this->date->addRule($required);
        $this->depart->addRule($required);
        $this->arrivee->addRule($required);
        $this->places->addRule($required);

        $lieu = $this->lieu;
        //rule location
        $rule = new Callback(function($value, &$error) use($lieu) {
            if ($lieu->getLieuIdByName($value) !== null)
                return true;
            $error = 'Lieu invalide : ' . $value;
            return false;
        });
        $this->depart->addRule($rule);
        $this->arrivee->addRule($rule);

        $check_time = new \system\form\rule\Callback(function($value, &$error) {
            $r = $this->strToTime($value);

            if (!$r) {
                $error = 'Date invalide : ' . array_values(\DateTime::getLastErrors()['errors'])[0];
                return false;
            }
            
            if($r->getTimestamp() <= time()){
                $error = 'La date doit être ultérieur à la date actuelle';
                return false;
            }

            return true;
        });

        $this->date->addRule($check_time);
        
        $this->places->addRule(new \system\form\rule\Regex('/^[0-9]{1,2}$/', 'Le nombre doit être valide'));

        //handle fields
        $this->addField($this->date);
        $this->addField($this->depart);
        $this->addField($this->arrivee);
        $this->addField($this->places);
    }
    
    
    /**
     * Convert a date string to DateTime
     * @param string $str_time The string representation of the date
     * @return \DateTime
     */
    public function strToTime($str_time){
        return \DateTime::createFromFormat('d/m/Y G:i', $str_time);
    }

    public function getID() {
        return 'traj_form';
    }

    public function getSubmitURL() {
        return 'trajets/add/ajax';
    }

    public function getValidationURL() {
        return 'trajets/validateadd';
    }

    //getters
    
    /**
     * 
     * @return \system\form\field\Field
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * 
     * @return \system\form\field\Field
     */
    public function getDepart() {
        return $this->depart;
    }

    /**
     * 
     * @return \system\form\field\Field
     */
    public function getArrivee() {
        return $this->arrivee;
    }


    /**
     * 
     * @return \system\form\field\Field
     */
    public function getPlaces() {
        return $this->places;
    }
}
