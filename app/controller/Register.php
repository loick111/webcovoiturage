<?php

/*
 * The MIT License
 *
 * Copyright 2014 Vincent Quatrevieux <quatrevieux.vincent@gmail.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace app\controller;

/**
 * Description of Register
 *
 * @author Vincent Quatrevieux <quatrevieux.vincent@gmail.com>
 */
class Register extends \system\mvc\Controller {
    /**
     *
     * @var \app\form\Register
     */
    private $form;

    /**
     * @var \app\model\Account
     */
    private $account;
    
    public function __construct(\system\Base $base, \app\form\Register $form, \app\model\Account $account) {
        parent::__construct($base);
        $this->form = $form;
        $this->account = $account;
    }
    
    public function indexAction(){
        $this->output->setLayoutTemplate('layout/big-logo.php');
        $this->output->setTitle('Inscription');
        $this->helpers->loadCSS('register');
        return $this->output->render('register/index.php', array('form' => $this->form));
    }
    
    public function scriptAction(){
        $this->output->setLayoutTemplate(null);
        $this->output->getHeader()->setMimeType('text/javascript');
        return $this->form->getJS();
    }
    
    public function validateAction($field = ''){
        $this->output->setLayoutTemplate(null);
        $this->output->getHeader()->setMimeType('text/json');
        $errors = array();
        $this->form->validate($field, $errors);
        
        return json_encode($errors);
    }
    
    public function performAction($method = 'html'){
        if($method === 'ajax'){
            $this->output->setLayoutTemplate(null);
            $this->output->getHeader()->setMimeType('text/json');
        }
        
        $errors = array();
        if($this->form->validate(null, $errors))
        {
            $prenom = $this->form->getFirstName()->getValue();
            $nom = $this->form->getLastName()->getValue();
            $statut = $this->form->getStatus()->getValue();
            $mail = $this->form->getMail()->getValue();
            $password = $this->form->getPass()->getValue();

            $this->account->addUser($mail, $password, $prenom, $nom, $statut, '', '');
            
            if($method === 'html')
                $this->output->getHeader()->setLocation($this->helpers->url('account/login.php'));
            else
                return <<<EOD
                {
                    "status":"SUCCESS",
                    "redirect": "{$this->helpers->url('account/login.php')}"
                }
EOD;
        }
        else
        {
            if($method === 'html')
                $this->output->getHeader()->setLocation($this->helpers->url('register'));
            else
                return json_encode ($errors);
        }

    }
}
