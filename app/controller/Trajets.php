<?php

/*
 * The MIT License
 *
 * Copyright 2014 loick.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace app\controller;

use system\error\Http404Error;

/**
 * Description of Account
 *
 * @author loick
 */
class Trajets extends \system\mvc\Controller {

    /**
     *
     * @var \app\model\Lieu
     */
    private $lieu;

    /**
     * @var \app\model\Trajet
     */
    private $trajet;

    /**
     * @var \app\form\Trajets
     */
    private $form;

    public function __construct(\system\Base $base, \app\model\Lieu $lieu, \app\model\Trajet $trajet, \app\form\Trajets $form) {
        parent::__construct($base);
        $this->lieu = $lieu;
        $this->trajet = $trajet;
        $this->form = $form;
    }

    public function addAction($method = 'html') {
        if (!$this->session->isLogged())
            $this->output->getHeader()->setLocation($this->helpers->url('account/login'));

        if ($method === 'html') {
            $this->output->setTitle('Ajout trajet');
            $this->output->setLayoutTemplate('layout/small-logo.php');
            $this->helpers->loadCSS('trajets');
        } else {
            $this->output->setLayoutTemplate(null);
            $this->output->getHeader()->setMimeType('text/json');
        }

        $errors = array();

        if ($this->form->validate(null, $errors)) {
            $depart = $this->lieu->getLieuIdByName($this->form->getDepart()->getValue());
            $arrivee = $this->lieu->getLieuIdByName($this->form->getArrivee()->getValue());
            $date = $this->form->strToTime($this->form->getDate()->getValue())->getTimestamp();
            $max = (int) $this->form->getPlaces()->getValue();

            $id = $this->trajet->addTrajet($this->session->getUserId(), $depart, $arrivee, $date, $max);

            $url = $this->helpers->secureUrl('trajets', 'show', $id);

            if ($method === 'html') {
                $this->output->getHeader()->setLocation($url);
            } else {
                return '{"status":"SUCCESS","redirect":"' . $url . '"}';
            }
        }

        if ($method === 'html')
            return $this->output->render('trajets/add.php', array('form' => $this->form));
        else
            return json_encode($errors);
    }

    public function validateaddAction($input = '') {
        $this->output->setLayoutTemplate(null);
        $this->output->getHeader()->setMimeType('text/json');
        $error = array();
        $this->form->validate($input, $error);
        return json_encode($error);
    }

    public function scriptaddAction() {
        $this->output->setLayoutTemplate(null);
        $this->output->getHeader()->setMimeType('text/javascript');
        return $this->form->getJS();
    }

    public function showAction($id = '') {
        $id = (int) $id;

        $trajet = $this->trajet->getTrajet($id);

        if (empty($trajet))
            throw new \system\error\Http404Error('Trajet introuvable');

        $trajet['reserves'] = $this->trajet->getReserves($id);
        
        $b = false;
        
        foreach($trajet['reserves'] as $r){
            if($r['user']['USER_ID'] == $this->session->getUserId()){
                $b = true;
                break;
            }
                
        }

        $this->output->setLayoutTemplate('layout/small-logo.php');
        $this->output->setTitle('Trajet');
        $this->helpers->loadCSS('trajets');
        return $this->output->render('trajets/show.php', array('trajet' => $trajet, 'canReserve' => $this->_canReserve($trajet), 'b' => $b));
    }

    public function reserveAction($id = '') {
        $id = (int) $id;

        if($this->session->isLogged()) {
            $trajet = $this->trajet->getTrajet($id);

            if (empty($trajet))
                throw new \system\error\Http404Error('Trajet introuvable');

            if(!$this->_canReserve($trajet))
                throw new \system\error\Http404Error('Résavation impossible');

            $this->trajet->addReserve($this->session->getUserId(), $id);
            $this->output->getHeader()->setLocation($this->helpers->secureUrl('trajets', 'show', $id));
        }
    }
    
    public function unreserveAction($id = ''){
        $id = (int)$id;
        if(!$this->session->isLogged())
            throw new \system\error\Http404Error('Veuillez vous connecter...');
        
        $this->trajet->unreserve($id, $this->session->getUserId());
        $this->output->getHeader()->setLocation($this->helpers->url('trajets/show/' . $id));
    }
    
    private function _canReserve(array $trajet){
        if(!$this->session->isLogged())
            return false;
        
        if($trajet['FREE_PLACES'] < 1)
            return false;
        
        if($trajet['USER_ID'] == $this->session->getUserId())
            return false;
        
        return !$this->trajet->getReserve($this->session->getUserId(), $trajet['TRAJET_ID']);
    }
    
    public function listAction(){
        if(!$this->session->isLogged())
            throw new \system\error\Http404Error('Veuillez vous connecter...');
        
        $this->output->setLayoutTemplate('layout/small-logo.php');
        $this->helpers->loadCSS('results');
        $this->output->setTitle('Mes trajets');
        
        return $this->output->render('trajets/list.php', array(
            'organized' => $this->trajet->getOrganizedTrajects($this->session->getUserId()),
            'part' => $this->trajet->getParticipatedTrajects($this->session->getUserId())
        ));
    }

}
