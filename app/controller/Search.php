<?php

/*
 * The MIT License
 *
 * Copyright 2014 loick.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace app\controller;

/**
 * Search controller
 *
 * @author loick
 */
class Search extends \system\mvc\Controller{
    /**
     * Googl map API model
     * @var \app\model\GoogleMapAPI
     */
    private $api;

    /**
     * Account model
     * @var \app\model\Account
     */
    private $account;
    
    /**
     *
     * @var \app\model\Trajet
     */
    private $trajet;
    
    public function __construct(\system\Base $base, \app\model\GoogleMapAPI $api, \app\model\Account $account, \app\model\Trajet $trajet) {
        parent::__construct($base);
        $this->api = $api;
        $this->account = $account;
        $this->trajet = $trajet;
    }
    
    public function indexAction(){
        $this->output->setTitle('Accueil');
        $this->output->setLayoutTemplate('layout/big-logo.php');
        $this->helpers->loadCSS('index');
        
        return $this->output->render('search/index.php');
    }
    
    public function contactAction() {
        $this->output->setTitle('Contact');
        $this->helpers->loadCSS('index');
        
        return $this->output->render('search/contact.php');
    }
    
    public function redirectAction(){
        if(empty($this->input->get->from) || empty($this->input->get->to))
            throw new \system\error\Http204NoContent();
        
        $this->output->getHeader()->setLocation($this->helpers->secureUrl(
                'search' , 'results',
                $this->input->get->from,
                $this->input->get->to . '.html'
        ));
    }

    public function resultsAction($from = '', $to = ''){
        $this->output->setTitle('Résultats de la recherche');
        $this->output->setLayoutTemplate('layout/small-logo.php');
        $this->helpers->loadCSS('results');

        $trajets = $this->trajet->trouverTrajet($from, $to);

        foreach($trajets as &$trajet)
            $trajet['user'] = $this->account->findUserById($trajet['USER_ID']);

        return $this->output->render('search/results.php', array(
            'from' => $from,
            'to' => $to,
            'results' => $trajets
        ));
    }
    
    public function autocompleteAction($terms = ''){
        $this->output->setLayoutTemplate(null);
        $this->output->getHeader()->setMimeType('text/json');
        
        if(empty($terms))
            return '[]';
        
        if(strlen($terms) > 32)
            $terms = substr ($terms, 0, 32);
        
        $data = $this->api->prediction($terms);
        
        $results = array();
        
        foreach($data['predictions'] as $prediction) {
            $results[] = $prediction['description'];
        }
        
        return json_encode($results);
    }
}
