<?php

/*
 * The MIT License
 *
 * Copyright 2014 loick.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace app\controller;

/**
 * Description of Account
 *
 * @author loick
 */
class Account extends \system\mvc\Controller{
    /**
     *
     * @var \app\model\Account
     */
    private $model;

    /**
     * @var \app\form\Profile
     */
    private $profile;

    public function __construct(\system\Base $base, \app\model\Account $model, \app\form\Profile $profile) {
        parent::__construct($base);
        $this->model = $model;
        $this->profile = $profile;
    }

    public function indexAction() {
        $this->output->setTitle('Mon compte');
        
        return $this->output->render('account/index.php');
    }
    
    public function loginAction($error = '') {
        $this->output->setTitle('Connexion');
        $this->output->setLayoutTemplate('layout/big-logo.php');
        $this->helpers->loadCSS('login');

        if($this->session->isLogged())
            $this->output->getHeader()->setLocation($this->helpers->url('search.php'));
        else
            return $this->output->render('account/login.php', array('error' => $error));
    }
    
    public function performloginAction($method = 'form'){
        if($method == 'ajax'){
            $this->output->setLayoutTemplate(NULL);
            //TODO: ajax login

        }else{
            $mail = $this->input->post->mail;
            $passwd = $this->input->post->password;

            $connect = $this->model->findUserByMailAndPassword($mail, $passwd);

            if($connect) {
                $this->session->login($connect['USER_ID']);
                return $this->output->getHeader()->setLocation($this->helpers->url('account/login/connected'));
            }

            return $this->output->getHeader()->setLocation($this->helpers->url('account/login/error'));
        }
    }

    public function profileAction() {
        $this->output->setTitle('Mon Compte');
        $this->output->setLayoutTemplate('layout/small-logo.php');
        $this->helpers->loadCSS('profile');

        if($this->session->isLogged())
            return $this->output->render('account/profile.php', array('form' => $this->profile));
        else
            return $this->output->getHeader()->setLocation($this->helpers->url('account/login'));
    }
    
    public function profilescriptAction(){
        $this->output->setLayoutTemplate(null);
        $this->output->getHeader()->setMimeType('text/javascript');
        return $this->profile->getJS();
    }

    public function disconnectAction() {
        $this->session->clear();
        $this->output->getHeader()->setLocation($this->helpers->url('account/login'));
    }
}
